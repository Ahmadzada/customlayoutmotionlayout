package com.example.custommontion.ui.customView

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.example.custommontion.R
import com.example.custommontion.databinding.CustomCardViewBinding

class AwesomeCustomView(
    context: Context,
    attributeSet: AttributeSet?
) : LinearLayout(context, attributeSet) {

    private val binding = CustomCardViewBinding.inflate(LayoutInflater.from(context), this, true)
    var profileNameAA = "ALma"
    var ProfileAgeAA = "89"

    init {
        inflate(context, R.layout.custom_card_view, this)
        context.theme.obtainStyledAttributes(attributeSet, R.styleable.AwesomeCustomView, 0, 0)
            .also {
                if (!it.getString(R.styleable.AwesomeCustomView_profileAge).isNullOrBlank()) {
                    binding.txtAge.text =
                        it.getInteger(R.styleable.AwesomeCustomView_profileAge, 0).toString()
                }
                if (!it.getString(R.styleable.AwesomeCustomView_profileName).isNullOrBlank()) {
                    binding.txtName.text =
                        it.getString(R.styleable.AwesomeCustomView_profileName).toString()
                }
            }
    }


}